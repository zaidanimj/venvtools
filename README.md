# Venvtools: Python Virtual Environments Management Made It Easy

## Introduction

venvtools provide support for Python’s 3.5 official lightweight virtual environments creation '*venv*' module. venvtools made managing development virtual environment easier via one command with series of actions.

Script coded with following values in mind:

* leveraging on default library/tools
* less dependency
* simple to operate

## Features

Currently venvtools support managing following virtual environment tasks: 

* [x]  **create** - Create virtual environment and project directory. Prepare isolated environment via '*venv*' Python module.
* [x]  **remove** - Remove virtual environment and project directory.
* [x]  **list** - List created virtual environment
* [x]  **activate** - Activate virtual environment and automatically change to project directory.
* [x]  **deactivate** - Deactivate current active virtual environment.
* [x]  **goto** - Change to selected directory of active environment; *environment*, *bin*, *sitepackages* and *project*. :new: :fire:

Todo:

* [ ]  **export** - Archive virtual environment plus project directory.
* [ ]  **import** - Deploy archived virtual environment.
* [ ]  **edit** - Edit pre/post activation/deactivation scripts.
* [ ]  **support pre/post activation** - Script sourced before/after the current environment is activated. 
* [ ]  **support pre/post deactivation** - Script sourced before/after the current environment is deactivated.

## Prerequisites

Following prerequisites to run venvtools:

* Bash 3.0 and above
* Python 3.5 and above
* Python venv module

### Supported Shell

venvtools is a set of shell functions scripted in linux shell. It was developed and tested under bash on MacOS High Sierra (10.13). 

Why bash? Most of enterprise linux distributions have bash as their default shell. What about zsh & ksh? So far no plan to support.

It may work with other shells, so if you find it does work with other than bash shell please let me know. If you can modify it to work with another shell, without completely rewriting it, let me know I will do my best to include it.

### Supported Python

Why Python 3.5 and above? venvtool leverage on Python *venv* module for creation of virtual environments. In Python version 3.5 module *venv*  included officially and recommended for creating virtual environments.

What about Python 2.7? The [End Of Life date (EOL, sunset date) for Python 2.7](http://legacy.python.org/dev/peps/pep-0373/) is on 2020. No plan to support Python 2.7 due EOL just around the conner and limited resources. 

## Installation

Following command will run installation script:

`curl -s https://gitlab.com/Fahmi.Salleh/venvtools/raw/master/install | bash`

For [manual installation](https://gitlab.com/Fahmi.Salleh/venvtools/wikis/installation#manual-installation) please refer to documentation.

## Documentation

See [wiki](https://gitlab.com/Fahmi.Salleh/venvtools/wikis/Home) for more information.

## Change Logs

Latest release:

##### v0.7.1 (2018-05-07) :new: :fire:

- Second rolling release.
- Support action *goto* - Change to predefined directory of active virtual environment.

See [change log v0.7.1](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.7.1)

See [full change logs](CHANGELOG.md).

##### v0.6.7 (2018-03-16)

- First rolling release.
- Support actions create, list, remove, activate, deactivate virtual environment.

See [change log v0.6.7](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.6.7)

See [full change logs](CHANGELOG.md).

## Download

Latest release: [v0.7.1](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.7.1)

Previous release: [v0.6.7](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.6.7)

Older release: [all](https://gitlab.com/Fahmi.Salleh/venvtools/tags)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Authors

* **Fahmi Salleh** (fahmie[at]gmail.com) - *Initial work* - 

## Acknowledgments

* Script coded due to laziness :sleepy: to type command here and there.
