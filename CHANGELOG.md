## v0.7.1 (2018-05-07)

* Second rolling release.

* Requirements:
    - [x] OS Linux or Darwin
    - [x] `bash >= 3.0.0`
    - [x] `python >= 3.5.0`
* New Features:
    - [x]  **goto** - Change to predefined directory of active virtual environment.

## v0.6.7 (2018-03-16)

* First rolling release.

* Requirements:
    - [x] OS Linux or Darwin
    - [x] `bash >= 3.0.0`
    - [x] `python >= 3.5.0`
* Features:
    - [x] **create** - Create virtual environment and project directory. Prepare isolated environment via '_venv_' Python module.
    - [x]  **remove** - Remove virtual environment and project directory.
    - [x]  **list** - List created virtual environment
    - [x]  **activate** - Activate virtual environment and automatically change to project directory.
    - [x]  **deactivate** - Deactivate current active virtual environment.
